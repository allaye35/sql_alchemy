
from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Department(Base):
    __tablename__ = 'department'
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    employees = relationship(
        'Employee',
        secondary='department_employee_link'
    )


class Employee(Base):
    __tablename__ = 'employee'
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    hired_on = Column(DateTime, default=func.now())
    departments = relationship(
        Department,
        secondary='department_employee_link'
    )


class DepartmentEmployeeLink(Base):
    __tablename__ = 'department_employee_link'
    department_id = Column(Integer, ForeignKey('department.id'), primary_key=True)
    employee_id = Column(Integer, ForeignKey('employee.id'), primary_key=True)
    extra_data = Column(String(256))
    department = relationship(Department, backref=backref("employee_assoc"))
    employee = relationship(Employee, backref=backref("department_assoc"))



from sqlalchemy import create_engine
engine = create_engine("mysql://root:root@localhost:3306/exercice1", echo=False)

from sqlalchemy.orm import sessionmaker
session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)

IT = Department(name="IT")
John = Employee(name="John")
John_working_part_time_at_IT = DepartmentEmployeeLink(department=IT, employee=John, extra_data='part-time')
s = session()
s.add(John_working_part_time_at_IT)
s.commit()


de_link = s.query(DepartmentEmployeeLink).join(Department).filter(Department.name == 'IT').first()
print("de_link.employee.name",de_link.employee.name)
print("de_link",de_link)
print("de_link.employee.name",de_link.employee.name)
de_link = s.query(DepartmentEmployeeLink).filter(DepartmentEmployeeLink.extra_data == 'part-time').first()
#for resultat in de_link:
#print(resultat.employee.name)
#commentaire
print(de_link.employee.name)

Bill = Employee(name="Bill")
IT.employees.append(Bill)
s.add(Bill)
s.commit()
